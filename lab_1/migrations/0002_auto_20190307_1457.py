# Generated by Django 2.1.1 on 2019-03-07 07:57

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('lab_1', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='portfolio',
            name='judul',
            field=models.CharField(max_length=50),
        ),
        migrations.AlterField(
            model_name='portfolio',
            name='kategori',
            field=models.CharField(choices=[('Pribadi', 'Pribadi'), ('Tugas Kuliah', 'Tugas Kuliah'), ('Tugas Kerja', 'Tugas Kerja')], default='Pribadi', max_length=100),
        ),
    ]
