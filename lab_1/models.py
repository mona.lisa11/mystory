from django.db import models

# Create your models here.
class Portfolio(models.Model):		
	CHOICES = (
		('Pribadi', 'Pribadi'),
		('Tugas Kuliah', 'Tugas Kuliah'),
		('Tugas Kerja', 'Tugas Kerja')
	)
	judul = models.CharField(max_length=50)
	tanggal_awal = models.DateTimeField(blank=True, null=True)
	tanggal_akhir = models.DateTimeField(blank=True, null=True)
	description = models.TextField()
	tempat = models.TextField()
	kategori = models.CharField(max_length=100, choices= CHOICES, default='Pribadi')
