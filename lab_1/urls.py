from django.urls import path
from .views import *
#url for app
urlpatterns = [
    path('', index, name="home"),
    path('portfolio/', portfolio, name="portfolio"),
    path('portfolio/input', portfolio_input, name="portfolio_input"),
    path('portfolio/delete/', delete_all, name="delete")
]
