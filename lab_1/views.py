from django.shortcuts import render
from datetime import datetime
from django.contrib import messages
from .form import PortfolioForm
from .models import Portfolio

response = {
    	'date' : datetime.now(),
}
def index(request):
    return render(request, 'home.html', response)



def portfolio_input(request):
	portfolio_form = PortfolioForm(request.POST or None)
	if (request.method == 'POST') :
		if (portfolio_form.is_valid()):
			portfolio_form.save()
			portfolio_form = PortfolioForm()
			messages.success(request, "New Portfolio successfully created.")
		else :
			messages.error(request, "Aw, Snap. You put the wrong input.")
	response['form'] = portfolio_form
	return render(request, 'portfolio_input.html', response)

def portfolio(request):
	portfolios = Portfolio.objects.all()
	response['portfolios'] = portfolios
	return render(request, 'portfolio.html', response)

def delete_all(request):
	Portfolio.objects.all().delete()
	response['portfolios'] = Portfolio.objects.all()
	messages.error(request, "All portfolio deleted.")
	return render(request, 'portfolio.html', response)
